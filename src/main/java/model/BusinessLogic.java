package model;

import java.io.IOException;

public class BusinessLogic implements Model {
    private Domain domain;
    String name="Olga";
    public BusinessLogic() {

        domain = new Domain("Solomia");
        add();
    }

    public void add(){
        int foo=0;
        try{
            Domain dom= new Domain("Solomia");
            if(foo == 0){
                throw new IOException("Exception in block 'try with resources'");
            }
            System.out.println("Hello my name is "+ dom.getName());
        } catch (IOException e) {
            System.out.println("Do not worry!");
        }
    }
    @Override
    public void close() throws Exception {
        domain.close();
    }

    @Override
    public String getName() {
            return domain.getName();
    }
}
