package model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class Domain implements AutoCloseable{
    private  String name;
    private int number;
    private static int count=0;
    private PrintWriter file;

    public Domain(String name){
        this.name = name;
        count++;
        number=count;
        System.out.println("Name: " + this.name);
        System.out.println("Number: " + this.number);
        try {
            file = new PrintWriter(this.name + ".txt");
            file.println("Hello!");
            file.println("Name: " + this.name);
            file.println("Number: " + this.number);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public static int getCount() {
        return count;
    }

    public void close() throws Exception {
        if (count == 3){
            throw new IOException("Exception: method 'close()'");
        }
        System.out.println("Close " + this.name);
        if(file !=null ) {
            file.println();
            file.close();
        }



    }
}
