import model.Domain;
import view.MyView;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException, InterruptedException {
        new MyView().show();

    }
}