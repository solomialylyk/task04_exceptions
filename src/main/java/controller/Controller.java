package controller;

public interface Controller {
    String getName();
    void close() throws Exception;
}
