package controller;
import model.*;

public class ControllerImpl implements  Controller {
    private Model model;
    public ControllerImpl(){ model= new BusinessLogic();}
    @Override
    public String getName() {
        return model.getName();
    }

    @Override
    public void close() throws Exception {
        model.close();
    }
}
